__________ is the main and the first step in school administration.
A. Organizing
B. Budgeting
C. Controlling
D. Planning
ANSWER: D

The _______ is the pivot of the educational system.
A. Teacher 
B. Principal
C. Support staff
D. Management
ANSWER: A

 _____________ is setting up of things and providing proper school environment,
A. Organizing 
B. Staffing
C. Coordination
D. Recording
ANSWER: A

 ____________contains the plan set for the school for the whole academic session.
A. School Record
B. School Calendar 
C. School Book
D. None of the above
ANSWER: B

The Educational Officer after inspection enters his observation about the school in ____________.
A. Log book
B. Service book
C. Observation book
D. Cash book
ANSWER: A

 The details of all the employees are captured in the _________________.
A. Log book
B. Service book
C. Observation book
D. Admission and Withdrawal book
ANSWER: B

_______ means recruitment of suitable staff for the institution as per its basic needs.
A. Organizing
B. Budgeting
C. Staffing
D. Recording
ANSWER: C

_______ refers to supervision of the whole process of administration.
A. Controlling
B. Organizing
C. Budgeting
D. Coordination
ANSWER: A
The number of wash basins should be in proportion of one to every _______ children.
A. fifteen
B. ten
C. five
D. thirty
ANSWER: B

The Educational Officer after inspection enters his observation about the school in _______.
A. log book
B. service book
C. cash book
D. record book
ANSWER: A

________ is a record book showing the details of each employee.
A. log book
B. service book
C. cash book
D. record book
ANSWER: B

_______ has two sides, credit side and debit side.
A. log book
B. service book
C. cash book
D. record book
ANSWER: C

 The main purpose of the supervision of teaching should be the _______.
A. Achievement of success in examination
B. Proper utilization of school facilities
C. Carrying out of the curriculum
D. Advancement of pupil welfare
ANSWER: D

Supervision should be primarily _______.
A. Constructive and creative
B. Preventive and corrective
C. Construction and critical
D. Privative and critical
ANSWER: A

The basic purpose of supervision is to help _______.
A. Teachers in dealing pupils
B. Children learn more effectively
C. Teachers in improving methods
D. Teachers in understanding pupil
ANSWER: B

The elementary school teachers are directly responsible to the _______.
A. Headmaster
B. Parents
C. Students
D. None of these
ANSWER: A

The most frequently used criticism for school administration is that _______.
A. They like praise
B. They are too lazy
C. They do not know teacher
D. They fail to provide leadership
ANSWER: D

The school headmaster is expected to _______.
A. Hold daily Prayers
B. Hold daily meetings
C. Prepare the budget
D. Put into operation the course of study
ANSWER: D

A supervisor is one who _______.
A. Gives orders
B. Provides friendly help
C. Inspects classrooms
D. Criticizes the teaching method
ANSWER: B

The effective supervision is indicated by _______.
A. Good relations between teacher and supervisors
B. Helping teachers becoming more self sufficient
C. Helping teacher in their teaching
D. Criticizing teacherís lessons
ANSWER: B

The chief responsibility of the principal is _______.
A. Maintain school records
B. Handle discipline problems
C. Provide leadership in instructional plan
D. Organize and administer the guidance programmed
ANSWER: C

 Administration means _______.
A. To run	
B. To protect
C. To establish
D. To look after
ANSWER: D

The function of educational administration and management is _______.
A. Instructional tasks
B. Non instructional tasks
C. Both a and b
D. None of the above
ANSWER: C

To make arrangements is the part of _______.
A. Planning
B. Organizing
C. Commanding
D. Coordinating
ANSWER: B

 School Budget includes
A. Non development expenditure
B. Development expenditure
C. Both a and b
D. None
ANSWER: C
