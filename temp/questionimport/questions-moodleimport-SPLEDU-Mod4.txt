IQ score of below _______ might indicate a problem.
A) 70
B) 60
C) 50
D) 80
ANSWER: A
Children with intellectual disability have problems with which type(s) of learning?
A) Academic
B) Experiential
C) Social
D) All of the above
ANSWER: D
Which of the following statement(s) are false?
A) Fetal Alcohol Syndrome (FAS) is one of the leading causes of intellectual disability.
B) Pre- and post-natal exposure to toxins can cause intellectual disabilities.
C) Consumption of fish rich in mercury can cause intellectual disabilities.
D) None of the above
ANSWER: D
ABA stands for _______.
A) Applied Behavioral Adaptation
B) Applied Behavioral Analysis
C) Application of Behaviour Analysis
D) None of the above
ANSWER: B
ABA approach uses which learning theory(s)?
A) Classical conditioning
B) Constructivist learning
C) Operant conditioning	
D) Both a and c
ANSWER: D
Slow learners score _______ on IQ tests.
A) between 70 and 90
B) between 70 and 80
C) between 80 and 90
D) between 90 and 100
ANSWER: A
_______ is an instructional approach that alters the presentation of content using learning centers and simulations, group discussions and co-operative learning.
A) Remedial teaching
B) Compensatory teaching
C) Direct instruction teaching
D) None of the above
ANSWER: B
_______ is an alternate approach for the regular classroom teacher in instructing the slow learner using activities, techniques and practices to eliminate weaknesses or deficiencies.
A) Remedial teaching
B) Compensatory teaching
C) Direct instruction teaching
D) None of the above
ANSWER: A
The ability to engage in problem solving, deduction, and complex memory tasks is called _______.
A) Communicative Competence
B) Cognitive Maturity
C) Developmental Error
D) Form-focused Instruction
ANSWER: B
An error in learner language, which does not result from first language influence but rather reflects the learners’ gradual discovery of the second language system is called _______.
A) Communicative Competence
B) Cognitive Maturity
C) Developmental Error
D) Form-focused Instruction
ANSWER: C
The ability to use language in a variety of settings, taking into account relationships between speakers and differences in situations is called _______.
A) Communicative Competence
B) Cognitive Maturity
C) Developmental Error
D) Form-focused Instruction
ANSWER: A
Instruction, which draws attention to the forms and structures of the language within the context of communicative interaction is called _______.
A) Communicative Instruction
B) Cognitive Instruction
C) Developmental Error
D) Form-focused Instruction
ANSWER: D
The process by which humans acquire the capacity to perceive and comprehend language as well as to produce and use words and sentences to communicate is called _______.
A) Communicative Competence
B) Language Acquisition
C) Language Acquisition Device
D) Form-focused Instruction
ANSWER: B
_______ is a controversial claim from language acquisition research proposed by Noam Chomsky in the 1960s.
A) Communicative Competence
B) Language Acquisition
C) Form-focused Instruction
D) Language Acquisition Device
ANSWER: D
Down's syndrome is a genetic disorder caused by the presence of all or part of a third copy of ________.
A) chromosome 21
B) chromosome 22
C) chromosome 19
D) chromosome 12
ANSWER: A
The average IQ of a young adult with Down syndrome is ________.
A) 60
B) 40
C) 50
D) 70
ANSWER: C
There are _______ basic types of Down’s syndrome.
A) four
B) five
C) six
D) three
ANSWER: D
In _______, although the total number of chromosomes is normal (46), a part of chromosome 21 breaks off, attaches to another chromosome, and produces the signs and features of Down syndrome.
A) Trisomy 21
B) Translocation
C) Mosaicism
D) None of the above
ANSWER: B
________ results from abnormal cell division in only some cells after fertilization, while others divide normally.
A) Trisomy 21
B) Translocation
C) Mosaicism
D) None of the above
ANSWER: C
_______ is a reduction process as the chromosome number of the final products is reduced from a full complement to one half.
A) Meiosis
B) Mitosis
C) Mosaicism
D) Trisomy
ANSWER: A
Possessing three copies of a particular chromosome instead of the normal two copies is referred to as _______.
A) Meiosis
B) Mitosis
C) Mosaicism
D) Trisomy
ANSWER: D
In _______, one cell divides evenly to produce two daughter cells that have the same chromosome number.
A) Meiosis
B) Mitosis
C) Mosaicism
D) Trisomy
ANSWER: B
_______ is a procedure performed between 10 and 12 weeks after a woman's last menstrual period to collect material for prenatal testing.
A) CVS
B) Chorionic villus sampling
C) Fried's diagnostic index
D) Both a and b
ANSWER: D
Which of the following statement(s) related to Down’s syndrome are false?
A) Several blood markers like β-hCG, inhibin-A, PAPP-A and unconjugated estriol can be used as part of combined tests to predict the risk of Down syndrome.
B) Ultrasound measurement of nuchal translucency is usually performed between 11 and 14 weeks gestation for assessing the risk of Down syndrome.
C) Decreased fetal nuchal translucency (NT) is an indicator of increased risk of Down syndrome.
D) None of the above
ANSWER: C
Which of the following are symptoms of Down’s syndrome?
A) Congenital heart disease and dental anomalies
B) Thyroid function abnormalities (hypo- or hyperthyroidism)
C) Unusual posturing of the head and neck
D) All of the above
ANSWER: D