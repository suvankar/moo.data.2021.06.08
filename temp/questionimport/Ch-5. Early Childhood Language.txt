To speak, a person must be able to control the voice mechanism and muscles of ________ and ____.
A. Tongue, mouth
B. Tongue, lips
C. Lips, mouth
D. Mouth, neck
ANSWER: B

Which of the following statements are true?
A. Communication is an interchange of thoughts, feelings, and emotions.
B. Speech is the expression of thoughts, feelings, and emotions, in words said aloud.
C. Only A
D. Both A and B
ANSWER: D

After babies are 2 or 3 months old, they can figure out the _________ states of others.
A. Emotional
B. Psychological
C. Physiological
D. None of the above
ANSWER: A

The things that parents can do with their children to significantly improve language development are:
A. Echoing, Expanding
B. Recasting, Labeling, Reading
C. None of the above
D. All of the above
ANSWER: D

_________ studies are defining the relationship between exposure to speech and language, brain development, and communication skills.
A. Brain imaging
B. Genetic studies
C. IQ tests
D. None of the above
ANSWER: A

Understanding the meanings of expressions and gestures start around the age of _____ months.
A. 3 months
B. 2 months
C. 4 months
D. 6 months
ANSWER: A

Pronouncing words is learned by ________
A. Speaking
B. Imitation
C. Gestures
D. Listening
ANSWER: B

The child learns to respond to bye –bye, names in the age group of ____
A. 12-18 months
B. 0-6 months
C. 6-12 months
D. 9-12 months
ANSWER: C

Babies first vocabulary are made up mostly of ________ and ______ , the most useful parts of speech.
A. Adjective, noun
B. Noun, verb
C. Verb, pronoun
D. Article, adjective
ANSWER: B

The infant is able to show the response of choosing one toy of its interest out of the several ones almost in the _____ month.
A. eighth
B. fifth
C. sixth
D. ninth
ANSWER: A

In case of babies suffering from colic, their cries are like ___________.
A. Low pitched cries.
B. Low pitched moaning with a feverish look.
C. Low pitched moaning broken by yawns, signs of sleepiness.
D. Piercing screams, abdomen expanded, pulling up and stiffening, legs.
ANSWER: D

The most intensive period of speech and language development for humans is during the first _____ years of life.
A. 3
B. 2
C. 4
D. 6
ANSWER: A

Speech sounds or building blocks that compose the words of a language are called _____.
A. Morphology
B. Physiology
C. Genetics
D. Phonemes
ANSWER: D

Language and speech are primarily learned through _______ .
A. Experimental approach
B. Trial, error method
C. Rule of thumb method
D. Imitation, observation
ANSWER: D

When child is learning to walk, their ______ development slows down.
A. talking
B. crawling
C. jumping
D. writing
ANSWER: A

During _________ babies pay close attention to their parents talk.
A. Childhood
B. Infancy
C. Adulthood
D. None of the above
ANSWER: B

Scientists have found that girls tend to start talking on average ___________ months earlier than boys
A. 2-3 months
B. 3-4 months
C. 1-2 months
D. 5-6 months
ANSWER: C

If a child is falling behind language development then the parents need to consult a _______
A. Speech pathologist
B. Language doctor
C. Speech-language therapist
D. Both A and C
ANSWER: D

A person’s language ability starts even before _______ and permeates into every field of his or her activity.
A. 1 year
B. 6 months
C. birth
D. 1 months
ANSWER: C

Babbling begins around the _______ month and reaches its peak when the baby becomes 9 months old.
A. Third
B. Second
C. Fifth
D. Sixth
ANSWER: A

Smacking lips or sticking the tongue out by babies indicates that the baby is:
A. Displeased
B. Hungry
C. Wants to be picked up
D. Thirsty
ANSWER: B

In the age of ______, a child has vocabulary of approx 150-300 words.
A. 3 years
B. 18 months
C. 2 years
D. 12 months
ANSWER: C

Communication is an interchange of thoughts,______ and emotions.
A. Feelings
B. Ideas
C. Awareness
D. Words
ANSWER: A


In the course of normal development, sound recognition, having been acquired at about ________ of age is followed a few months later by verbal recognition.
A. 3 months
B. 2 months
C. 6 months
D. 1 month
ANSWER: A

The major part of the baby’s vocabulary is made up of:
A. Nouns
B. Verbs
C. Articles
D. Adjectives
ANSWER: A
