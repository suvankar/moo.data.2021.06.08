﻿Maths activities can be broadly divided into _________ categories.
A. five
B. seven
C. six
D. eight
ANSWER: A

Montessori took the idea that the human has a mathematical mind from the French philosopher, ________.
A. Froebel
B. Piaget
C. Pascal
D. Vygotsky
ANSWER: C

Number rods are ten rods identical with the _______ in length, but divided into _______ and _______ sections. 
A. Brown Stairs, red, blue
B. Red Rods, red, green
C. Red Rods, red, blue
D. None of the above
ANSWER: C

The exercise, Sandpaper Numbers is suitable for age group _________.
A. 2-3 years
B. 2.5-3.5 years
C. 4 years and above
D. 2.5-4 years
ANSWER: C

The purpose of the activity, Number Rods and Cards is:
A. To relate the symbols to the quantities that the child knows and introduce the sequence of the symbols.
B. To prepare for addition, subtraction, and multiplication
C. To keep the child engaged in random games so that he doesn’t disturb the class
D. Both A and B
ANSWER: D

Spindle Box contains ________ slots with a total of ________ spindles.
A. 9, 45
B. 9, 40
C. 8, 40
D. None of the above
ANSWER: A

The Memory Game is used to ________.
A. To reinforce the concept of tens.
B. To reinforce the concept of teens.
C. To reinforce the concept of zero.
D. None of the above
ANSWER: C

For the exercise, Introduction to quantity, we will need a supply box with _______ beads, _______ bars, _______ square and _______ cube
A. ten, unit, hundred, thousand
B. hundred, thousand, unit, ten
C. thousand, hundred, ten, unit
D. unit, ten, hundred, thousand
ANSWER: D

Which of the following statement is false about the exercise, Symbols?
A. box containing 4 sets of cards is needed, 1 set for units, tens, hundreds and thousands.
B. its purpose is to acquaint the child with the written symbols for the quantities of the decimal system. 
C. It is suitable for ages 4-4.5 years.
D. none of the above
ANSWER: D

The exercises, Number Rods and Cards, Spindle Boxes fall under _________.
A. Numbers through Tens exercises
B. Decimal system exercises
C. Tables of Arithmetic exercises
D. Passage to Abstraction exercises
ANSWER: A

For the exercise, Formation of Numbers, we will need the supply of limited golden bead materials and the large cards to ________.
A. Hundreds
B. Thousands
C. Ten thousands
D. None of the above
ANSWER: B

The exercise, Changing is suitable for age group _________.
A. 2-3 years
B. 3.5-4.5 years
C. 4.5 years
D. None of the above
ANSWER: C

“To connect the arithmetic problems to real life and illustrate practical problems.” is the purpose of the exercise, _________.
A. Word Problems
B. Addition
C. Division
D. Changing
ANSWER: A

For the exercise, Dot Game, a ruler and squared paper inserted into a frame of ground glass or slate with columns headed ________ is needed.
A. 1, 10, 100 and 1,000
B. 1, 10, 100, 1,000, 10,000 and 100,000
C. 1, 10, 100, 1,000, and 10,000
D. None of the above
ANSWER: C

The exercises, Addition Charts 3 to 6 and Subtraction Snake Game fall under _________.
A. Numbers through Tens exercises
B. Decimal system exercises
C. Tables of Arithmetic exercises
D. Passage to Abstraction exercises
ANSWER: C

Which of the following statements are true regarding the exercise, Addition Snake Game?
A. To familiarize the child with all the possible number combinations that make up ten. 
B. To emphasize the fact that in addition, you will be making tens. 
C. To reinforce the knowledge that addition and multiplication are of the same concept.
D. All of the above
ANSWER: D

The exercise, Small Bead Frame: introduction to addition, subtraction and multiplication is suitable for age group _________.
A. 3-4 years
B. 4-5 years
C. 5.5-6 years
D. None of the above
ANSWER: C

Which of the following statements are false about the exercise, Wooden Hierarchical Material?
A. It consists of one piece of Thousand : green cube with red divisions 5cm by 5cm by 5cm. 
B. It consists of one piece of Ten Thousand : blue prism with green divisions 5cm by 5cm by 5cm
C. It consists of one piece of Million : green cube with red divisions 50cm by 50cm by 50cm.
D. None of the above
ANSWER: D

The exercises, Division and Stamp Game fall under _________.
A. Numbers through Tens exercises
B. Decimal system exercises
C. Tables of Arithmetic exercises
D. Passage to Abstraction exercises
ANSWER: B

The categories of Maths activities are:
A. Numbers through Ten, The Decimal System, 
B. Counting beyond Ten, Memorization of the arithmetic tables, 
C. Passage to abstraction, Fractions
D. All of the above
ANSWER: D

The exercises, Small Bead Frame and Wooden Hierarchical Material fall under _________.
A. Numbers through Tens exercises
B. Decimal system exercises
C. Tables of Arithmetic exercises
D. Passage to Abstraction exercises
ANSWER: D

Which of the following statements are true about the purpose of the exercise, Large Bead Frame?
A. To show the relationship between categories of the decimal  system. 
B. To clarify position and place value. 
C. Practice with reading and writing of large numbers.
D. All of the above
ANSWER: D

For the activity, Fractions, we will need _______ fraction circles in _______ frames, consisting of _______ circles – 1 undivided and the others divided into 2 to 10 equal part
A. red, green, ten
B. green, red, ten
C. green, red, eight
D. none of the above
ANSWER: A

The materials used in the Montessori Maths activities ________.
A. Does not promote concrete learning that can help the child is his later development.
B. Are un-appealing to the child because they are dull and uncolorful.
C. Are not imaginative, auto-didactic, and does not support concrete learning.
D. None of the above
ANSWER: D

Choose the correct options for the activity, Fractions.
A. Its purpose is to help the child gain a sensorial impression of fraction.
B. Its purpose is the Introduction to the concept and notation of fractions and simple operations.
C. Its purpose is Sensorial exploration of equivalency among fraction.
D. All of the above
ANSWER: D
