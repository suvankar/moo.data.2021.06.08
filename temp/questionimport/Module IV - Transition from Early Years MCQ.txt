Children in ________ setting encounter different people at various service within a particular day.
A) vertical
B) horizontal
C) diagonal
D) none of the above
ANSWER: B

Moving from one activity to another within a program can be referred as _______ transitions.
A) vertical
B) horizontal
C) diagonal
D) none of the above
ANSWER: A

_______ is a process of moving  from  one  stage  to another in a sequential manner which is associated to an appropriate age.
A) Growth
B) Development
C) Maturation
D) All of the above
ANSWER: B

Theorists like_______, moral development and _______, psycho-social development have spoken about the transition process.
A) Lawrence Kohlberg, Erick Erickson
B) Erick Erickson, Lawrence Kohlberg
C) Lawrence Kohlberg, John Dewey
D) Froebel, Erick Erickson
ANSWER: A
According to the theory on _______ by Jean Piaget, children or an individual develops one's own cognition and progressively transforming from one stage to another.
A) scaffolding
B) constructivism
C) ecology
D) none of the above
ANSWER: B

________ is explained as a course of change, where an individual adjusts to a novel environment.
A) Change
B) Development
C) Transition
D) All of the above
ANSWER: C

According  to  _______,  an  individual  move  from  simple  tasks  to complex  tasks  and  they  gradually  attain  complex  or  sophisticated  capacities  in thinking and reasoning.
A) Piaget
B) Vygotsky
C) Dewey
D) Maria Montessori
ANSWER: A

During the _______, educational literature emphasized on the concept of readiness.
A) 1820s
B) 1920s
C) 1930s
D) 1940s
ANSWER: B

_______ learning   refers   to   the   various   ways   in   which   caregiver   and communities  aid  children  to  attain  the  mastery  over  culture,  societal  norms  and valued  behaviours.
A) Social
B) Socio-economical
C) Socio-cultural
D) Psycho-social
ANSWER: C

In _______ view of  transition,  the  children  are  active  participants in  the  timing  and quality of transition experiences.
A) Piagetian
B) Kindergarten
C) Constructivism
D) Vygotskian
ANSWER: D

Piaget suggested that children develop schemes to  represent  their understanding of the world around  them and  they  assimilate  the new  concepts  into  these  schemes  until  takes  place
A) balance
B) equilibration
C) both a and b
D) none of the above
ANSWER: B

The assistance received from the peers and adults develop into a new goal and this process is called _______.
A) scaffolding
B) constructivism
C) ZPD
D) All of the above
ANSWER: A

16. Perspectives on transition divides influencing factors into groups _______.
A) internal and external factors
B) individual and family factors
C) both a and b
D) none of the above
ANSWER: B

According to Lev Vygotsky, transition between learning and development occurs in a stage _______, it is a difference between what a child can do without help and what he or she can do with the help or support rendered.
A) Zone of proximal development
B) ZPD
C) Both a and b
D) None of the above
ANSWER: C

_______ forms a lifelong grid in the human life through which all people move gradually from known into unknown realms of experience.
A) Transition
B) Change
C) Both a and b
D) None of the above
ANSWER: A

Transitions can be organized according to ________.
A) age
B) stage
C) type of school
D) all of the above
ANSWER: D

Kagan described transition into ________ types.
A) three
B) four
C) two
D) five
ANSWER: C

The factors which can effect the transition process are _______.
A) Preschool or day care centers
B) Socio-economic status of parents
C) Childs resiliency
D) All of the above
ANSWER: D

According to _______, a school or a preschool that supports childs transition is one that offers quality education; services and is one which will recognize  and  adapt  the  local  needs.
A) Myers and Landers
B) Maurice and Goldhaber
C) Frelburg
D) Eddington
ANSWER: A

What is defined as a minimum standard or attainment to few parameters?
A) Teaching
B) Education
C) Quantity
D) Quality
ANSWER: D

Absence in smooth transition can be a precursor to _______.
A) low grades in higher classes
B) happy in classroom
C) academic excellence
D) feeling comfortable in classroom
ANSWER: A

Which approach emphasizes in the concept of holistic and integrated development?
A) Interactional approach
B) Maturational approach
C) Environmentalist approach
D) Integrationist approach
ANSWER: D

Transition is a mutual preparedness of children, _______ and _______.
A) community and environment
B) school and teachers
C) parents and school
D) teachers and parents
ANSWER: C

Quality  education  can  be  defined  in  many ways, it is one that is _______.
A) Relevant and purposeful
B) Inclusive
C) promotes equality and equal participation
D) all of the above
ANSWER: D

According to a study conducted by _______, suggests that parents confidence in teachers and school affects the childs transition process.
A) Myers and Landers
B) Maurice and Goldhaber
C) Fabian and Dunlop
D) Eddington
ANSWER: C

The process of assimilating the gained information to fit into existing schemes is referred to as _______.
A) Equilibration
B) Accommodation
C) Adaptation
D) Assimilation
ANSWER: D

_______ is considered in terms of certain abilities, skills and behaviours that will indicate childrens preparedness to formal schooling.
A) Preparedness
B) School readiness
C) ZPD
D) All of the above
ANSWER: B

________ is not defined in terms of functioning in one area of development but rather having a set of skills and behaviours across different domains of development.
A) Integrationist approach
B) Interactional approach
C) Maturational approach
D) None of the above
ANSWER: A

_______ suggest children are ready to start school when they reach a level of maturity, this allows them to cope with the school system.
A) Integrationist approach
B) Interactional approach
C) Maturational approach
D) None of the above
ANSWER: C

A comprehensive approach which incorporates thr childs overall development and environmental effects is _______.
A) Integrationist approach
B) Interactional approach
C) Maturational approach
D) None of the above
ANSWER: B

According to the ecological theory by Urie Bronfenbrenner, the eco system is divided into various  parts such as,
A) microsystem and mesosystem,
B) exosystem and macrosystem
C) both a and b
D) none of the above
ANSWER: B

_______ is a technique or an approach that parents adopt to address to the needs and challenges of a child, allows children to be treated with respect and teaches children without using any punitive methods.
A) Authoritative parenting
B) Authoritarian parenting
C) Positive parenting
D) Permissive parenting
ANSWER: C

The building blocks of positive parenting are:
A) Mutual respect between children and caregivers
B) Attempt to understand the motive behind a behavior
C) Effective communication and encouragement
D) All of the above
ANSWER: D

The barriers to positive parenting are:
A) Vulnerable families
B) Limited early childhood experiences
C) Effective communication
D) Both a and b
ANSWER: D

_______ is the accurate processing of sensory stimulation in the environment, it is the way the brain receives signals to organize and respond to the stimulation received.
A) Sensory processing
B) Executive functioning
C) Self-regulation
D) Emotional-regulation
ANSWER: A

_______ is the higher order of thinking and reasoning skills, these skills allows an individual  to  plan,  execute,  complete  a  task,  understand  different  perspectives, regulate emotions, starting an activity and staying focused.
A) Sensory processing
B) Executive functioning
C) Self-regulation
D) Emotional-regulation
ANSWER: B

Griebel and Niesel describes the change at various levels as children enter formal schooling, these are:
A) individual level
B) interactive level
C) contextual level
D) All of the above
ANSWER: D

_______ suggest that children who had difficulty in transition, will also have difficulties  in  making   friends,   adjusting   to   new   environment,   may   also   have emotional and health problems.
A) Kagan and Neuman
B) Myers and Landers
C) Maurice and Goldhaber
D) Fabian and Dunlop
ANSWER: A

_______ is the main component in the process of transition.
A) Preparedness
B) ZPD
C) School readiness
D) All of the above
ANSWER: C

The main areas of executive function are:
A) working memory
B) cognitive flexibility
C) inhibitory control
D) all of the above
ANSWER: D
