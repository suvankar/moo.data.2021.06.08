Waldorf curriculum approach was developed by _______.
A) Fortson and Reiff
B) Rudolf Steiner
C) Sawyer and Sawyer
D) Rosegrant and Bredekamp
ANSWER: B

Waldorf curriculum approach was proposed in the year_______.
A) 1920
B) 1919
C) 1918
D) 1921
ANSWER: B

Which of the following statement(s) are true about Waldorf curriculum approach?
A) Rudolf Steiner stressed to teachers that the best way to provide meaningful support for the child is to comprehend these phases fully and to bring "age appropriate" content to the children that nourishes healthy growth.
B) Waldorf teachers strive to transform education into an art that educates the whole child.
C) Waldorf curriculum is broad and comprehensive, structured to respond to the three developmental phases of childhood.
D) All of the above
ANSWER: D

Bank Street Approach is known as ________ because it is concerned with the interaction of all areas of a childs development.
A) Developmental Interactionist model
B) Developmental Integrated model
C) Developmental Interconnected model
D) None of the above
ANSWER: A

Cognitively Oriented High Scope Approach was developed under the leadership of ________.
A) Bereiter and Engelman
B) David Weikart
C) Coplan and Arbeau
D) Schiller
ANSWER: B

High Scope Approach was based on the cognitive development theory of ________.
A) Lev Vygotsky
B) John Dewey
C) Froebel
D) Jean Piaget
ANSWER: D

High Scope Approach was initially designed for preschool children during the ________ when there was a social concern for the welfare of impoverished children.
A) 1950s
B) 1960s
C) 1970s
D) 1980s
ANSWER: B

The transformational curriculum model was proposed by ________.
A) Sawyer and Sawyer
B) Rosegrant and Bredekamp
C) John Dewey
D) Froebel
ANSWER: B

Project approach was proposed by ________.
A) Sawyer and Sawyer
B) Rosegrant and Bredekamp
C) Katz and Chard
D) Froebel
ANSWER: C

Which of the following statement(s) are true about the Project approach?
A) A project is defined as an in-depth investigation of a real world topic worthy of childrens attention and effort. The Project Approach is a clearly structured, three-phase scientific exploration of a topic of interest.
B) The topic of the project has to be stated in the form of question rather than as a single word
C) After the topic selection, the teacher has to engage the children in discussion to know their personal experiences and knowledge of topic through talking, writing or drawing.
D) All of the above
ANSWER: D

Emergent curriculum approach was proposed by ________.
A) Sawyer and Sawyer
B) Rosegrant and Bredekamp
C) Jones and Nimmo
D) Katz and Chard
ANSWER: C

Which of the following statement(s) are true?
A) There is no difference between Emergent curriculum approach and traditional thematic approach.
B) Emergent curriculum approach differs from traditional thematic approach.
C) While traditional thematic approach is based on predetermined themes for a set time period, Emergent curriculum, on the other hand makes use of themes that are important and relevant to children.
D) Both b and c
ANSWER: D

Sources of emergent curriculum are:
A) Childrens and Teachers interests
B) Development tasks and Serendipity
C) Physical and Social environment
D) All of the above
ANSWER: D

Open structures integrated learning approach was proposed by ________.
A) Fortson and Reiff
B) Sawyer and Sawyer
C) Rosegrant and Bredekamp
D) Katz and Chard
ANSWER: A

Which of the following statement(s) are true about Open structures integrated learning approach?
A) Holds the belief that human beings, whether adults or children function at their best when their behavior is creative and responsible.
B) Based on theories and educational philosophies of Bruner, Dewey, Erikson Maslow, Piaget, Vygotsky and others.
C) The overriding feature of this approach is to build feelings of competence and eagerness for learning in each child.
D) All of the above
ANSWER: D

_______ immerses children in real-world themes that begin with what is familiar to children and connects to the world around them.
A) Bank Street Approach
B) Scholastic Early Childhood Program
C) Cognitively Oriented High Scope Approach
D) Integrated whole Language Curriculum Approach
ANSWER: B

Which of the following statement(s) are true about Scholastic Early Childhood Program?
A) The SECP curriculum is thematic, integrated, and replete with culturally-relevant materials in both English and French.
B) Curriculum goals of SECP are implemented through organization around real-world themes.
C) Central goal of the program is to help guarantee that young children will develop the critical skills, knowledge, and life habits to become successful learners, especially in their language and literacy development.
D) Both b and c
ANSWER: D

Which of the following statement(s) are true about the Direct Instruction Approach?
A) focus on teacher-directed activities, with carefully planned lessons, drills and exercises for practicing important concepts.
B) Teachers use constant reinforcement to motivate childrens participation with the use of both positive words of praise and tangible rewards.
C) Students are grouped according to their achievement, teachers are provided with closely scripted lesson plans, students respond to the teacher orally and as a group, and the group does not move on until everyone understands the material.
D) All of the above
ANSWER: D

________ is grounded in the philosophy of John Dewey and other early educational progressive workers, who were influenced by Freuds theoretical claims of profound formative influence of early childhood years.
A) Direct Instruction Approach
B) Bank Street Approach
C) Cognitively Oriented High Scope Approach
D) Integrated whole Language Curriculum Approach
ANSWER: B

Direct Instruction Approach was developed by ________.
A) Bereiter and Engelman
B) David Weikart
C) Coplan and Arbeau
D) Schiller
ANSWER: A

________ is based on behavioral principles of cause and effect, stimulus and response and behavior modification.
A) Direct Instruction Approach
B) Bank Street Approach
C) Cognitively Oriented High Scope Approach
D) Integrated whole Language Curriculum Approach
ANSWER: A

Which of the following statement(s) are true about the Bank Street Approach?
A) The childs environment and experiences are considered the foundation of curriculum, which is explored in depth.
B) The classroom environment and materials under this approach are arranged into conventional interest areas.
C) It emphasizes short term goals rather than specific long term objectives.
D) Both a and b
ANSWER: D

________ uses whole texts and literature for literacy learning and facilitating a child-centered atmosphere, cooperative learning and parent involvement in learning.
A) Direct Instruction Approach
B) Bank Street Approach
C) Cognitively Oriented High Scope Approach
D) Integrated whole Language Curriculum Approach
ANSWER: D

________ holds the conviction that children should be given the freedom, encouragement, and guidance necessary for emergency and reinforcement of their creative and problem-solving responses.
A) Open structures integrated learning approach
B) Transformational Curriculum Approach
C) Bank Street Approach
D) Cognitively Oriented High Scope Approach
ANSWER: A

________ is a cohesive, comprehensive Prekindergarten program that is structured around the pedagogy of systematic, developmentally-appropriate curriculum, effective instructional methodology, and attention to cognitive and affective skill development.
A) Transformational Curriculum Approach
B) Bank Street Approach
C) Cognitively Oriented High Scope Approach
D) Scholastic Early Childhood Program (SECP)
ANSWER: D

_______ is based on a profound understanding of human development that addresses the needs of the growing child.
A) Transformational Curriculum Approach
B) Waldorf Education
C) Bank Street Approach
D) Cognitively Oriented High Scope Approach
ANSWER: B

Which of the following statement(s) are true?
A) The teacher is at the center stage in the educative process, and the most important factor in the learning environment.
B) The learner is at the center stage in the educative process, and the most important factor in the learning environment.
C) Teachers should remember that there is no best strategy that could work in a million of different student background and characteristics.
D) Both b and c
ANSWER: D

Bank Street Approach was founded by________.
A) Lucy Sprague Mitchell
B) Bereiter and Engelman
C) David Weikart
D) Coplan and Arbeau
ANSWER: A

Bank Street Approach was founded in the year ________.
A) 1915
B) 1917
C) 1916
D) 1910
ANSWER: C

Direct Instruction Approach was founded in the year ________.
A) 1950
B) 1970
C) 1960
D) 1980
ANSWER: C

________model is based on meaning-centered, integrated, mindful curriculum with whole child focus and individual development.
A) Transformational Curriculum Approach
B) Bank Street Approach
C) Cognitively Oriented High Scope Approach
D) Integrated whole Language Curriculum Approach
ANSWER: A

_______ refers to a set of teaching strategies which enable teachers to guide children through in-depth studies of real-world topics.
A) Transformational Curriculum Approach
B) Bank Street Approach
C) Cognitively Oriented High Scope Approach
D) The Project Approach
ANSWER: D

The Waldorf curriculum is structured to respond to the three developmental phases of childhood:
A) from birth to approximately 3 years, from 4 to 6 years and from 7 to 8 years.
B) from birth to approximately 4 years, from 5 to 8 years and from 8 to 10 years.
C) from birth to approximately 6 or 7 years, from 7 to 14 years and from 14 to 18 years.
D) None of the above
ANSWER: C

Transformational Curriculum Approach was founded in the year ________.
A) 1992
B) 1991
C) 1990
D) 2000
ANSWER: A

The _________ curriculum focuses on children?s learning resulting from common interests.
A) Subject centered
B) Child centered
C) Activity centered
D) Integrated
ANSWER: B

_________ is central to the childs well-being and development.
A) Exploration
B) Experiment
C) Play
D) School
ANSWER: C

High scope model provides a way for teachers to extend cognitive development of children through key experiences in ________ concept areas.
A) six
B) four
C) eight
D) seven
ANSWER: C

________ refers to development of the ability to order, compare and match objects.
A) Spatial relationships
B) Seriation
C) Classification
D) Active learning
ANSWER: B

Integrated whole Language approach was developed by ________.
A) Sawyer and Sawyer
B) John Dewey
C) Froebel
D) Jean Piaget
ANSWER: A

Integrated whole Language approach was based on the body of research called ________.
A) emergent linguistics
B) emergent numeracy
C) emergent literacy
D) none of the above
ANSWER: C
