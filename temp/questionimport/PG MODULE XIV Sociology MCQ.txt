How many major methods of investigation in social phenomenon used by sociology?
A) 5
B) 6
C) 7
D) 8
ANSWER: D

Beginning in the mid-eighteenth century, social changes took place that are associated today with the concept of modernity. Which of the following responses best describes a major consequence of those changes?
A) the expansion of personal choice
B) the creation of cohesive communities
C) the enrichment of spiritual life
D) the decline of social stratification
ANSWER: A

A sociology teacher who is seeking to deepen student understanding of the sociological concepts of norms, values, and mores could best do so by having students:
A) assess the influence of leaders on various social movements.
B) examine how social status affects social order.
C) analyze factors that lead to conformity and non-conformity.
D) discuss ways in which role expectations can lead to role conflict.
ANSWER: C

The key to a successful life is to control the diversities and diversions of life and achieve the desired values:
A) Mental and spiritual growth
B) Achievements of the values of life
C) Development of conduct and moral character
D) Development of balanced personality.
ANSWER: B

________ is mathematics applied to human facts:
A) Statistical method
B) Social Research
C) Social statistics
D) Both a and c
ANSWER: D

According to the philosophy of idealism:
A) spiritual idea, values are more important than mere material property
B) in a materialistic society, more importance to give material prosperity.
C) do not believe in the ultimate reality of spiritualism like Idealists.
D) none of the above
ANSWER: A

In the words of Plato :
A) Civil society is a mind
B) Civil society is a mind writ large
C) The personality is formed , maintained and changed as the socialization process moves along
D) Socialization is in the family
ANSWER: B

The family plays perhaps :
A) An unimportant role in the individuals socialization
B) The most important role in the individuals socialization
C) No role in the individuals socialization
D) None of the above.
ANSWER: B

The improvement of socialization offers one of the greatest possibilities for the future alteration of human nature and human society, as quoted by:
A) Ross
B) Plato
C) Akolkar
D) Davis
ANSWER: D

Which of the following statement(s) are true about the Case study method?
A) It is an all-inclusive and intensive study of an individual, in which the investigator brings to bear all his skill and methods
B) It is a systematic gathering of enough information about a person to pursue one to understand how he or she functions as a unit of society.
C) It is a way of organizing social data so as to preserve the unitary character of the social object being studied.
D) All of the above
ANSWER: D

According to Groves:
A) Children are in greater need of individual attention, love and satisfaction of response.
B) The protection and care of children is one of the necessary function of the family.
C) The family keeps the social heritage intact and hands it over to the generations to the next.
D) Both B and C
ANSWER: D

Family performs many economic functions, they are :
A) To establish status, socialization, social control
B) Procuring of food, housing and clothing.
C) Division of labor, provision for income, Organization and care of property.
D) Religious functions.
ANSWER: C

In the modern family the most difficult problem is that of the :
A) Mutual adjustment of husband and wife.
B) Mutual adjustment brother and sister
C) No mutual adjustment among family members
D) Mutual adjustment of uncle and aunty.
ANSWER: A

A major problem faced by the modern family is an increase in the :
A) Number of modern families
B) Number of broken marriages
C) Number of increased family members
D) Lower birth rate
ANSWER: B

The family creates an environment in which the child can be brought up to develop fully his personality and to :
A) Become an ideal citizen
B) Become  an engineer
C) Become a doctor
D) Become no body of the family.
ANSWER: A

War is one cause of social disorganization because it introduces :
A) Cultural lag
B) Social changes
C) Confusion and disorder in society
D) Lack of family unity.
ANSWER: C

Which of the following statement(s) are false about Social Disorganization?
A) It is the process by which the relationships between members of a group are broken or dissolved.
B) It is disturbance in the patterns and mechanisms of human relation.
C) It is a decrease of the influence of existing social rules of behavior upon individual members of the group.
D) None of the above
ANSWER: D

There is a very intimate relation between :
A) Social group and social organization
B) Both a and c
C) Progressive and non progressive society
D) None of the above.
ANSWER: A

________ is the science of the behavior of the individual in society.
A) Social physiology
B) Social psychology
C) Social morphology
D) Social methodology
ANSWER: B

The methods of Sociology and Economics are:
A) Identical
B) Different in units
C) Different in attitude
D) Not identical
ANSWER: D

What is the name of the process by which we acquire a sense of identity and become members of society?
A) rationalization
B) colonization
C) McDonaldization
D) socialization
ANSWER: D

In contemporary societies, social institutions are:
A) highly specialized, interrelated sets of social practices
B) disorganized social relations in a postmodern world
C) virtual communities in cyberspace
D) no longer relevant to sociology
ANSWER: A

Which of the following is not recognised as a level of society?
A) the household
B) the office
C) the global village
D) the nation state
ANSWER: B

When sociologists study the structure of layers in society and people's movement between them, they call this:
A) social stratification
B) social control
C) social conflict
D) social solidarity
ANSWER: A

Social norms are:
A) creative activities such as gardening, cookery and craftwork
B) the symbolic representation of social groups in the mass media
C) religious beliefs about how the world ought to be
D) rules and expectations about interaction that regulate social life
ANSWER: D

In idealized views of science, the experimental method is said to involve:
A) testing out new research methods to see which one works best
B) isolating and measuring the effect of one variable upon another
C) using personal beliefs and values to decide what to study
D) interpreting data subjectively, drawing on theoretical paradigms
ANSWER: B

Society cannot be studied in the same way as the natural world because:
A) human behaviour is meaningful, and varies between individuals and cultures
B) it is difficult for sociologists to gain access to a research laboratory
C) sociologists are not rational or critical enough in their approach
D) we cannot collect empirical data about social life
ANSWER: A

Sociology differs from common sense in that:
A) it focuses on the researchers' own experiences
B) it makes little distinction between the way the world is and the way it ought to be
C) its knowledge is accumulated from many different research contexts
D) it is subjective and biased
ANSWER: C

It is generally believed to be necessary that:
A) There should exist a blood relationship between husband and wife.
B) Blood relationship between husband and wife does not matter.
C) There should exist no blood relationship between husband and wife.
D) None of the above.
ANSWER: C

There are two types of family :
A) Basic and universal family
B) Universal and versatile family
C) Basic and universal , Traditional
D) Universal and versatile family
ANSWER: C

According to the Hindu Shastras :
A) Procreation is the main function of the family.
B) Working in kitchen is the main function is family.
C) Cooking and cleaning floors are the main function in the family.
D) Working and earning money is the main function of the family.
ANSWER: A

Sociology can be considered a social science because:
A) its theories are logical, explicit and supported by empirical evidence
B) sociologists collect data in a relatively objective and systematic way
C) ideas and research findings are scrutinized by other sociologists
D) all of the above
ANSWER: D

Interconnection, bi-directionality, and synchronization tend to be strongest in which of the following forms of socialization?
A) the organizational socialization of new employees in a firm
B) the secondary socialization of the student-teacher relationship
C) the resocialization of individuals entering total institutions
D) the reciprocal socialization of the mother-infant relationship
ANSWER: D

A primary group can best be defined as a group:
A) that is characterized by shared interests and interchangeability of roles.
B) in which two or more people interact in predictable ways.
C) that is characterized by face-to-face interaction and close emotional ties.
D) in which two or more people work together to achieve a goal.
ANSWER: C

Functionalists would most likely argue that social stratification is universal because of the need to:
A) prevent low-status groups from disrupting the social status quo.
B) motivate qualified people to fill important positions in society.
C) preserve distinctions between low-status and high-status social groups.
D) regulate the pace and scope of social mobility in a society.
ANSWER: B

Sociology  may defined as study of society , according to :
A) L.F. Ward
B) T. Abel
C) K. Motwani
D) Ginsberg.M
ANSWER: D

There are two main schools of thought among the sociologists on scope and subject matter of sociology:
A) Specialistic or Formalistic and Synthetic
B) Specialistic or Synthetic and Formalistic
C) Both a and b
D) None of the above
ANSWER: A

Durkheim has divided the subject matter of sociology into:
A) five parts
B) two parts
C) four parts
D) three parts
ANSWER: D

_______ consists of the study of factors such as law, religion, fashion, mores etc, which exercise some kind of control:
A) Social pathology
B) Social process
C) Social control
D) Social morphology
ANSWER: C

Sociologists influenced by conflict theory would most likely endorse which of the following explanations of social control?
A) Sanctions benefit those in power by contributing to the maintenance of existing forms of social stratification.
B) Establishment of a clearly understood system of penalties and rewards is necessary to maintain social order.
C) Social norms must be carefully monitored if society is to function smoothly.
D) Institutional maintenance of social order provides people the assurance they need to establish meaning in their lives.
ANSWER: A
