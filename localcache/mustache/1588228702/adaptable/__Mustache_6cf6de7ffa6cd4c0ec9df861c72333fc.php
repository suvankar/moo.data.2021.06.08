<?php

class __Mustache_6cf6de7ffa6cd4c0ec9df861c72333fc extends Mustache_Template
{
    private $lambdaHelper;

    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $this->lambdaHelper = new Mustache_LambdaHelper($this->mustache, $context);
        $buffer = '';

        $buffer .= $indent . '<div class="book p-4">
';
        $buffer .= $indent . '    <div class="text-right">';
        $value = $this->resolveValue($context->find('printdialoglink'), $context);
        $buffer .= $value;
        $buffer .= '</div>
';
        $buffer .= $indent . '    <div class="text-center p-b-1 book_title">';
        $value = $this->resolveValue($context->find('booktitle'), $context);
        $buffer .= $value;
        $buffer .= '</div>
';
        $buffer .= $indent . '    <div class="book_info w-100 p-t-3 d-inline-block">
';
        $buffer .= $indent . '        <div class="w-50 float-left">
';
        $buffer .= $indent . '            <table>
';
        $buffer .= $indent . '                <tr>
';
        $buffer .= $indent . '                    <td>
';
        $buffer .= $indent . '                        ';
        // 'str' section
        $value = $context->find('str');
        $buffer .= $this->section6352e994d4253e0dee2a6c8b8054fabc($context, $indent, $value);
        $buffer .= ':
';
        $buffer .= $indent . '                    </td>
';
        $buffer .= $indent . '                    <td class="p-l-1">
';
        $buffer .= $indent . '                        ';
        $value = $this->resolveValue($context->find('sitelink'), $context);
        $buffer .= $value;
        $buffer .= '
';
        $buffer .= $indent . '                    </td>
';
        $buffer .= $indent . '                </tr>
';
        $buffer .= $indent . '                <tr>
';
        $buffer .= $indent . '                    <td>
';
        $buffer .= $indent . '                        ';
        // 'str' section
        $value = $context->find('str');
        $buffer .= $this->section935207dbb135155d9033286c71d96189($context, $indent, $value);
        $buffer .= ':
';
        $buffer .= $indent . '                    </td>
';
        $buffer .= $indent . '                    <td class="p-l-1">
';
        $buffer .= $indent . '                        ';
        $value = $this->resolveValue($context->find('coursename'), $context);
        $buffer .= $value;
        $buffer .= '
';
        $buffer .= $indent . '                    </td>
';
        $buffer .= $indent . '                </tr>
';
        $buffer .= $indent . '                <tr>
';
        $buffer .= $indent . '                    <td>
';
        $buffer .= $indent . '                        ';
        // 'str' section
        $value = $context->find('str');
        $buffer .= $this->sectionD27115497abe622f62e52617071c05fb($context, $indent, $value);
        $buffer .= ':
';
        $buffer .= $indent . '                    </td>
';
        $buffer .= $indent . '                    <td class="p-l-1">
';
        $buffer .= $indent . '                        ';
        $value = $this->resolveValue($context->find('modulename'), $context);
        $buffer .= $value;
        $buffer .= '
';
        $buffer .= $indent . '                    </td>
';
        $buffer .= $indent . '                </tr>
';
        $buffer .= $indent . '            </table>
';
        $buffer .= $indent . '        </div>
';
        $buffer .= $indent . '        <div class="w-50 float-left">
';
        $buffer .= $indent . '            <table class="float-right">
';
        $buffer .= $indent . '                <tr>
';
        $buffer .= $indent . '                    <td>
';
        $buffer .= $indent . '                        ';
        // 'str' section
        $value = $context->find('str');
        $buffer .= $this->sectionA165670204dee01521977b35ed6073eb($context, $indent, $value);
        $buffer .= ':
';
        $buffer .= $indent . '                    </td>
';
        $buffer .= $indent . '                    <td class="p-l-1">
';
        $buffer .= $indent . '                        ';
        $value = $this->resolveValue($context->find('username'), $context);
        $buffer .= $value;
        $buffer .= '
';
        $buffer .= $indent . '                    </td>
';
        $buffer .= $indent . '                </tr>
';
        $buffer .= $indent . '                <tr>
';
        $buffer .= $indent . '                    <td>
';
        $buffer .= $indent . '                        ';
        // 'str' section
        $value = $context->find('str');
        $buffer .= $this->sectionC82794fede2c493af84f404702c7b9db($context, $indent, $value);
        $buffer .= ':
';
        $buffer .= $indent . '                    </td>
';
        $buffer .= $indent . '                    <td class="p-l-1">
';
        $buffer .= $indent . '                        ';
        $value = $this->resolveValue($context->find('printdate'), $context);
        $buffer .= $value;
        $buffer .= '
';
        $buffer .= $indent . '                    </td>
';
        $buffer .= $indent . '                </tr>
';
        $buffer .= $indent . '            </table>
';
        $buffer .= $indent . '        </div>
';
        $buffer .= $indent . '    </div>
';
        // 'bookintro' section
        $value = $context->find('bookintro');
        $buffer .= $this->section2c82afe17a6f964821ebc333daebde7a($context, $indent, $value);
        $buffer .= $indent . '    <div class="w-100">
';
        $buffer .= $indent . '        <div class="p-b-2 p-t-2">';
        $value = $this->resolveValue($context->find('toc'), $context);
        $buffer .= $value;
        $buffer .= '</div>
';
        $buffer .= $indent . '    </div>
';
        $buffer .= $indent . '    <div class="w-100">
';
        // 'chapters' section
        $value = $context->find('chapters');
        $buffer .= $this->section45433a68a649148a9bd4cae4cdea9b47($context, $indent, $value);
        $buffer .= $indent . '    </div>
';
        $buffer .= $indent . '</div>
';

        return $buffer;
    }

    private function section6352e994d4253e0dee2a6c8b8054fabc(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' site ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' site ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section935207dbb135155d9033286c71d96189(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' course ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' course ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionD27115497abe622f62e52617071c05fb(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' modulename, mod_book ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' modulename, mod_book ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionA165670204dee01521977b35ed6073eb(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' printedby, booktool_print ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' printedby, booktool_print ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionC82794fede2c493af84f404702c7b9db(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' printdate, booktool_print ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' printdate, booktool_print ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section0b257672dfa14e719ae30834047be479(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' description ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' description ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section2c82afe17a6f964821ebc333daebde7a(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
    <div class="w-100 book_description">
        <div class="p-b-2 p-t-2">
            <h2 class="text-center p-b-2">{{#str}} description {{/str}}</h2>
             <p class="book_summary">{{{ bookintro }}}</p>
        </div>
    </div>
    ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '    <div class="w-100 book_description">
';
                $buffer .= $indent . '        <div class="p-b-2 p-t-2">
';
                $buffer .= $indent . '            <h2 class="text-center p-b-2">';
                // 'str' section
                $value = $context->find('str');
                $buffer .= $this->section0b257672dfa14e719ae30834047be479($context, $indent, $value);
                $buffer .= '</h2>
';
                $buffer .= $indent . '             <p class="book_summary">';
                $value = $this->resolveValue($context->find('bookintro'), $context);
                $buffer .= $value;
                $buffer .= '</p>
';
                $buffer .= $indent . '        </div>
';
                $buffer .= $indent . '    </div>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionD49fcd1f8850097acaac3cd5d3745677(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
            <div class="p-b-2">
            {{{ content }}}
            </div>
        ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '            <div class="p-b-2">
';
                $buffer .= $indent . '            ';
                $value = $this->resolveValue($context->find('content'), $context);
                $buffer .= $value;
                $buffer .= '
';
                $buffer .= $indent . '            </div>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section45433a68a649148a9bd4cae4cdea9b47(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
        {{#visible }}
            <div class="p-b-2">
            {{{ content }}}
            </div>
        {{/visible}}
    ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                // 'visible' section
                $value = $context->find('visible');
                $buffer .= $this->sectionD49fcd1f8850097acaac3cd5d3745677($context, $indent, $value);
                $context->pop();
            }
        }
    
        return $buffer;
    }

}
