<?php

class __Mustache_de1b03540dbb0aadbd6f8c9557e977e4 extends Mustache_Template
{
    private $lambdaHelper;

    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $this->lambdaHelper = new Mustache_LambdaHelper($this->mustache, $context);
        $buffer = '';

        $buffer .= $indent . '<div class="chapter col-12 p-4">
';
        $buffer .= $indent . '    <div class="text-right">';
        $value = $this->resolveValue($context->find('printdialoglink'), $context);
        $buffer .= $value;
        $buffer .= '</div>
';
        $buffer .= $indent . '    <div class="text-center p-b-2">';
        $value = $this->resolveValue($context->find('booktitle'), $context);
        $buffer .= $value;
        $buffer .= '</div>
';
        $buffer .= $indent . '    <div class="chapter">
';
        // 'parentchaptertitle' section
        $value = $context->find('parentchaptertitle');
        $buffer .= $this->section635dc77c10b729b101c0bc8f71d10aa2($context, $indent, $value);
        $buffer .= $indent . '        ';
        $value = $this->resolveValue($context->findDot('chapter.content'), $context);
        $buffer .= $value;
        $buffer .= '
';
        $buffer .= $indent . '    </div>
';
        $buffer .= $indent . '</div>
';

        return $buffer;
    }

    private function section635dc77c10b729b101c0bc8f71d10aa2(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
         <div class="text-center">
            {{{parentchaptertitle}}}
         </div>
        ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '         <div class="text-center">
';
                $buffer .= $indent . '            ';
                $value = $this->resolveValue($context->find('parentchaptertitle'), $context);
                $buffer .= $value;
                $buffer .= '
';
                $buffer .= $indent . '         </div>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

}
