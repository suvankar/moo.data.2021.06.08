________ refers to integrating learners in the regular school system (and class) as opposed to placing a learner in a special school or class.
A) Integration
B) Inclusion
C) Scaffolding
D) Remediation
ANSWER: B
________ is a systematic process of gathering relevant and valid information about a learner’s strengths and needs.
A) Psycho-educational assessment
B) Remediation assessment
C) Remediation assessment
D) None of the above
ANSWER: A
________ focuses on finding alternative ways to help, mainly, primary school age children learn.
A) Integration
B) Inclusion
C) Scaffolding
D) Remediation
ANSWER: D
________ refers to breaking up new concepts so that they can be learned more easily.
A) Integration
B) Inclusion
C) Scaffolding
D) Remediation
ANSWER: C
________ is an umbrella term that includes assistive, adaptive, and rehabilitative devices for people with disabilities. 
A) Assistive technology
B) Inclusion
C) Scaffolding
D) Remediation
ANSWER: A
________ refers to adjusting assessments, material, curriculum, or classroom environment to accommodate a student's needs so that the child can participate in, and achieve the teaching-learning goals. 
A) Enhancement 
B) Individualization
C) Adaptation
D) Elaboration
ANSWER: C
________ are modifications that relate specifically to instruction or content of a curriculum. 
A) Curriculum adjustments
B) Curriculum adaptations
C) Curriculum 
D) None of the above
ANSWER: B
Response to Intervention (RTI) is a useful tool for teachers because it _______.
A) immediately identifies students with learning disabilities.
B) addresses the needs of English language learners.
C) provides a systematic approach to identifying students' needs.
D) allows students without an IEP to receive special services.
ANSWER: C
The Individuals with Disabilities Education Act (IDEA) provides a free and appropriate public education to all children with a disability. What else is provided to all students under IDEA?
A) Preferential classroom assignment
B) Transportation to the school of their choice
C) Instruction in the least restrictive environment
D) A paraprofessional aide to assist with instruction
ANSWER: C
The practice of fully integrating all students into classroom instruction regardless of race, gender, religion, socioeconomic status, ethnicity, physical or mental ability, or language is known as ________.
A) Inclusion
B) Response to Intervention
C) Differentiated Instruction
D) Awareness of Exceptionalities
ANSWER: A
Under IDEA, students can qualify to receive services if they demonstrate a disability in one of 13 specific categories. Which of the following is not one of the categories?
A) Severe asthma
B) Emotional disturbance
C) Other health impairment
D) Orthopedic impairment
ANSWER: A
The least restrictive environment mandate under IDEA requires that students with the most severe disabilities receive a free and appropriate public education with their typical peers ________.
A) at least three times per week
B) at least eight times per month
C) to the greatest extent possible
D) at the discretion of the IEP team
ANSWER: C
The Individualized Education Program (IEP) must be based solely on ________.
A) the student's needs
B) pre-existing programs
C) services available in the district
D) recommendations of the general educator
ANSWER: A
An IEP must include all of the following except ________.
A) current academic and functional performance of the student
B) annual academic and functional goals that are measurable
C) times when the student will not participate with their nondisabled peers
D) times when the student will not participate with their peers with a disability
ANSWER: D
Which of the following is a required component of the student's Individualized Education Program (IEP)?
A) Functional Behaviour Assessment
B) Behaviour Intervention Plan
C) Daily class schedule
D) Annual goals
ANSWER: D
Which of the following is the best example of differentiating instruction?
A) Exempting half the class from a homework assignment.
B) Assigning different students to read certain chapters of a Read aloud book.
C) Asking all the boy to make a poster and all the girls to write an essay.
D) Allowing the students to summarize the chapter with a poem, essay or cartoon.
ANSWER: D
Under IDEA which of the following age-groups are qualified to receive early intervention services?
A) 0-2 years
B) 3-4 years
C) 5-6 years
D) 7-8 years
ANSWER: A
Which of the following rewards would be appropriate for fifth-grade students who have demonstrated appropriate classroom behaviour?
A) Giving them ice-creams as an afternoon treat
B) Assigning them no homework for an entire week
C) Excusing them from the next chapter test
D) Providing them with extra computer time
ANSWER: D
Since the 1970s, the numbers of children in categories of disabilities that are most easily defined, such as physical impairments, have _______.
A) increased for elementary-school-aged students, while declining for high-school-aged students.
B) have remained fairly stable
C) increased, due to advances in medical care
D) decreased, due to advances in medical care
ANSWER: B
A program tailored to the needs of a learner with exceptionalities is called _______.
A) A set of learning objectives (SLO)
B) An individualized education program (IEP)
C) A cognitive development plan (CDP)
D) A special education plan (SED)
ANSWER: B
In a classic study, placement in general education classes rather than special-education classes _______.
A) resulted in a high dropout rate for students who are emotionally disturbed and educable mentally retarded.
B) resulted in higher achievement levels over 3 years for students who are emotionally disturbed and educable mentally retarded.
C) resulted in lower achievement levels over 3 years for students who are emotionally disturbed and educable mentally retarded.
D) made no difference in academic achievement when compared with a control group of students who were in special-education classes.
ANSWER: B
How might a teacher help a student with a very limited attention span (one perhaps with ADHD, in the regular classroom) to focus on a lecture and to organize concepts?
A) Make a student understand all classroom rules and procedures
B) Adhere to the principles of effective classroom management
C) Allow students who are hyperactive to have many opportunities to be active
D) All of the Above
ANSWER: D
Placement of students in the "least restrictive" educational environment was developed as a result of _______.
A) Evaluation of students
B) Efforts to conduct historical research
C) Efforts to normalize the lives of children with disabilities
D) Persistence in scientific experimentation
ANSWER: C
What is an effective way to structure peer tutoring to help meet the needs of students with disabilities?
A) Train the peer tutors into procedures of modeling and explaining.
B) Train the peer tutors in how to give specific positive and correct feedback.
C) Train the peer tutors about when and when not to allow students to work alone.
D) All of the Above
ANSWER: D
What did the 2004 update on IDEA support?
A) The correction of racial disparities in assignment to special education
B) The approval of schools to spend special education funds to prevent children from needing special education services
C) It changed the definition of learning disabilities to eliminate the concept of discrepancy between IQ and achievement.
D) All of the Above
ANSWER: D